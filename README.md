# Fireblocks Challenge:
>Build a RESTful microservice that can receive msgs and serve them using long polling method.
The service should allow clients to send msgs to other clients while allowing the clients to receive the messages using long polling method.
The timeout can be set by the client or default to 20 seconds.
A client that sends a message will receive a requestId.
The service should also support querying the status of this requestId.     
>   
>Design the solution so it would be easy to replace any storage with a DB connection.   
>
>Write a short description regarding each REST endpoint.


## Dependencies:
- [libx.js](https://github.com/Livshitz/libx.js) - multi-purpose toolbox with plenty of helpers, wrappers and useful modules
- [express](https://github.com/expressjs/express) - http web server
- [jest](https://github.com/facebook/jest) - tests


## Setup:
* Install dependencies:   
```yarn install```

* Build before running:   
```yarn build```


## How to use:
- Start server   
`yarn server`

- Run tests:   
```yarn test```   


## Endpoints:
- `[POST] /message/`   
Post new message. Expects to get new message JSON in body (e.g: {senderId, recipientId, content})
- `[GET] /message/is-exists/:requestId?`  
Peeks to check if a message for the given requestId exists
- `[GET] /message/:clientId[?timeout=<seconds>]`   
Connect and wait for a message. Will timeout after 20s (by default), unless provided in query

** Default endpoing is: [http://localhost:5678/](http://localhost:5678/  )

## Develop:
* Run build and watch:   
```yarn watch```
