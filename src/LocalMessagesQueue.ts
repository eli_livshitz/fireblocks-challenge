import { Message } from "./Message";
import { IMessagesQueueProvider } from './IMessagesQueueProvider';

export class LocalMessagesQueue implements IMessagesQueueProvider<Message> {
	private storage: LibxJS.Map<Message[]> = {};
	private requestIdToMessage: LibxJS.Map<Message> = {}; // For quick lookup (requestId->msg)

	public async push(msg: Message): Promise<void> {
		if (this.storage[msg.recipientId] == null)
			this.storage[msg.recipientId] = [];
		this.storage[msg.recipientId].push(msg);

		this.requestIdToMessage[msg.requestId] = msg; // just points to same place in memory, no duplication
	}
	
	public async dequeue(clientId: string): Promise<Message> {
		let arr = this.storage[clientId];
		if (arr == null || arr.length == 0)
			return null;

		let ret = arr.shift();
		delete this.requestIdToMessage[ret.requestId];
		return ret;
	}

	public async isExists(requestId: string): Promise<boolean> {
		return this.requestIdToMessage[requestId] != null;
	}
}
