export interface IMessagesQueueProvider<T> {
	push(data: T): Promise<void>;
	dequeue(key: string): Promise<T>;
	isExists(requestId: string): Promise<boolean>;
}
