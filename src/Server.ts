// Deps & deps config
global.libx = require('libx.js/bundles/essentials');
libx.node = require('libx.js/node');
libx.log.isShowStacktrace = false;
import express = require('express');
const bodyParser = require('body-parser');

// Modules
import { Message } from "./Message";
import { LocalMessagesQueue } from './LocalMessagesQueue';

class Server {
	private app: express.Application;
	private server: any;
	private messages: LocalMessagesQueue;
	private onMessageReceived: LibxJS.ICallbacks;
	public options: ServerOptions = {
		port: 5678,
		endpoint: null,
		verbose: false,
		timeoutSec: 20,
	};
	
	constructor(options?: ServerOptions) {
		this.app = express();
		this.app.use(bodyParser.json());

		this.options = libx.extend(options, this.options);
		this.options.endpoint = `http://localhost:${this.options.port}/`;

		this.messages = new LocalMessagesQueue();
		this.onMessageReceived = new libx.Callbacks();
	}

	public async serve(): Promise<void> {
		let p = libx.newPromise();

		this.app.post('/message/', async (req, res) => {
			let msg: Message = req.body;
			libx.log.verbose('request "/message[POST]":', msg);
			msg.createDate = new Date();
			msg.requestId = libx.newGuid();
			await this.messages.push(msg);
			this.onMessageReceived.trigger();
			return res.status(200).send(msg.requestId);
		});

		this.app.get('/message/is-exists/:requestId?', async (req, res) => {
			let requestId = req.params.requestId;
			libx.log.verbose('request "/message/is-exists/:requestId?":',  requestId);

			let ret = await this.messages.isExists(requestId);
			
			return res.status(200).send(ret);
		});

		this.app.get('/message/:clientId', async (req, res) => {
			let clientId = req.params.clientId;
			let timeoutSec = req.query.timeout || this.options.timeoutSec
			libx.log.verbose('request "/message/:clientId":', clientId);

			let msg = await this.messages.dequeue(clientId);
			if (msg != null) return res.status(200).json(msg);

			let isResolved = false;
			let isDropped = false;
			let until = this.onMessageReceived.until(async ()=>{
				libx.log.v('/message/:clientId/: onMessageReceived was triggered', clientId);
				let msg = await this.messages.dequeue(clientId);
				if (msg != null) {
					until();
					isResolved = true;
					libx.log.info('/message/:clientId/: found relevant message, responding now', clientId, msg);
					return res.status(200).json(msg);
				}
			})
			libx.sleep(timeoutSec * 1000).then(()=>{
				until();
				if (!isResolved && !isDropped) {
					libx.log.warning('/message/:clientId/: Timeout passed without matching response.', clientId);
					return res.status(500).send('TIMEOUT');
				}
			});

			// Handle request that closed unexpectedly
			req.on("close", function() {
				until();
				isDropped = true;
				libx.log.v('/message/:clientId/: request was closed unexpectedly', clientId);
				return res.end();
			});
			
			// NOTICE: no response if not found (long polling)...
		})
		
		this.server = this.app.listen(this.options.port, () => {
			libx.log.info('------------------------------');
			libx.log.info(`Local express server listening on ${this.options.endpoint}`);
			libx.log.info('------------------------------');
			p.resolve(this.options.endpoint);
		})
	
		return p;
	}

	public stop(): void {
		this.server.close();
	}
}

class ServerOptions {
	port?: number;
	endpoint?: string;
	verbose?: boolean;
	timeoutSec?: number;
};

if (libx.node.isCalledDirectly()) {
	let server = new Server();
	server.options.verbose = true;

	libx.node.onExit(()=> {
		console.log('server:cli: shutting down server...')
		server.stop();
	});
	
	(async () => {
		await server.serve();
		console.log('ready!');
	})();
}

export = Server;
