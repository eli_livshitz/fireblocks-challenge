export class Message {
	public senderId: string;
	public recipientId: string;
	public content: string;
	public requestId?: string;
	public createDate?: Date;
}
