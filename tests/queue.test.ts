import { LocalMessagesQueue } from "../src/LocalMessagesQueue";
import { Message } from "../src/Message";

let queue: LocalMessagesQueue;
let msg: Message = {
	content: 'content',
	createDate: new Date(),
	recipientId: 'recipientId',
	requestId: 'requestId',
	senderId: 'senderId',
}

beforeEach(()=>{
	queue = new LocalMessagesQueue();
})

test('push', async () => {
	await queue.push(msg);
});
test('push and peek by clientId', async () => {
	await queue.push(msg);
	expect(await queue.isExists(msg.requestId)).toEqual(true);
});
test('peek non existing requestId', async () => {
	expect(await queue.isExists('non-existing-requestId')).toEqual(false);
});
test('push and dequeue', async () => {
	await queue.push(msg);
	expect(await queue.isExists(msg.requestId)).toEqual(true);
	let deqMsg = await queue.dequeue(msg.recipientId);
	expect(await queue.isExists(msg.requestId)).toEqual(false);
	expect(deqMsg).toEqual(msg);
});
