import { Message } from "../src/Message";
import Server = require("../src/Server");
require('libx.js/modules/network');

libx.log.filterLevel = libx.log.severities.fatal;

let timeoutSec = 1;
let server: Server;
let msg: Message = {
	content: 'content',
	createDate: new Date(),
	recipientId: 'recipientId',
	requestId: 'requestId',
	senderId: 'senderId',
}

let helper = {
	push: async(msg: Message): Promise<string> => {
		let res = await libx.di.modules.network.httpPost(server.options.endpoint + '/message/', msg);
		if (res != null) return res.toString();
		return null;
	},
	isExists: async(requestId): Promise<boolean> => {
		return await libx.di.modules.network.httpGetString(server.options.endpoint + '/message/is-exists/' + requestId);
	},
	waitForMessage: async(clientId): Promise<Message> => {
		return await libx.di.modules.network.httpGetJson(`${server.options.endpoint}/message/${clientId}?timeout=${timeoutSec}`);
	}
}

interface IServer {
	isExists(requestId: string): Promise<string>;
	waitForMessage(): Promise<Message>;
}

beforeEach(()=>{
	server = new Server({
		timeoutSec: timeoutSec,
		endpoint: 'http://localhost:5678/',
	});
	server.serve();
})
afterEach(()=>{
	server.stop();
})

test('push message', async () => {
	let requestId = await helper.push(msg);
	expect(requestId).not.toBe(null);
});
test('push message and check existence', async () => {
	let existsBefore = await helper.isExists(msg.requestId);
	expect(existsBefore).toEqual("false");

	let requestId = await helper.push(msg);

	let existsAfter = await helper.isExists(requestId);
	expect(existsAfter).toEqual("true");
});
test('push message and dequeue', async () => {
	let requestId = await helper.push(msg);
	let existsBefore = await helper.isExists(requestId);
	expect(existsBefore).toEqual("true");

	let retMsg = await helper.waitForMessage(msg.recipientId);
	let diff = libx.diff(retMsg, msg);
	// Expect the returned message be same as the fake one here, except for the create date and actual requestId as they are overridden by the server.
	expect(diff).toEqual({ createDate: retMsg.createDate, requestId: retMsg.requestId });

	let existsAfter = await helper.isExists(requestId);
	expect(existsAfter).toEqual("false");
});
test('wait for message and push', async () => {
	let p = libx.newPromise();
	helper.waitForMessage(msg.recipientId).then(async retMsg=>{
		expect(retMsg).not.toBe(null);
		p.resolve();
	});

	let requestId = await helper.push(msg);
	let existsAfter = await helper.isExists(requestId);
	expect(existsAfter).toEqual("false");

	return p;
})
test('wait for message and timeout', async () => {
	let p = libx.newPromise();
	
	libx.measure('timeout');
	helper.waitForMessage(msg.recipientId).catch(async ex=>{
		let dur = libx.getMeasure('timeout');
		expect(dur).toBeGreaterThanOrEqual(timeoutSec * 1000);
		expect(dur).toBeLessThanOrEqual(timeoutSec * 1000 + 100);
		expect(ex).toEqual({ statusCode:500, response: 'TIMEOUT'});
		p.resolve();
	});

	return p;
})
